package ma.octo.assignement.service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.util.List;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

  @Mock
  private AccountRepository accountRepository;

  @InjectMocks
  private AccountService accountService;

  @Test
  void test_listAllAccounts() {
    final Account account = new Account();
    account.setNrCompte("nrCompe");
    final User user = new User();
    user.setUsername("username");
    account.setUtilisateur(user);
    account.setSolde(new BigDecimal("1000.00"));
    account.setRib("rib");
    account.setId(1L);
    Mockito.when(accountRepository.findAll()).thenReturn(List.of(account));

    final AccountDto result = accountService.listAllAccounts().get(0);

    assertEquals("username", result.getUsername());
    assertEquals("nrCompe", result.getNrCompte());
    assertEquals("rib", result.getRib());
    assertEquals(1L, result.getId());
    assertEquals(new BigDecimal("1000.00"), result.getSolde());
  }
}