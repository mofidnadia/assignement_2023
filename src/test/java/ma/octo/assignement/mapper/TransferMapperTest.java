package ma.octo.assignement.mapper;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.dto.TransferDto;
import org.junit.jupiter.api.Test;

class TransferMapperTest {

  @Test
  void testTransferMap() {
    // Given
    final Transfer transfer = new Transfer();
    transfer.setMontantTransfer(new BigDecimal("100.00"));
    transfer.setMotifTransfer("Buy Book");
    final Account accountBeneficiare = new Account();
    accountBeneficiare.setNrCompte("010000A000001000");
    final User user = new User();
    transfer.setDateExecution(new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime());

    accountBeneficiare.setUtilisateur(user);
    transfer.setCompteBeneficiaire(accountBeneficiare);
    final Account accountEmetteur = new Account();
    accountEmetteur.setNrCompte("010000B025001000");
    transfer.setCompteEmetteur(accountEmetteur);

    // When
    final TransferDto result = TransferMapper.map(transfer);

    // Then
    assertEquals(new BigDecimal("100.00"), result.getMontant());
    assertEquals("Buy Book", result.getMotif());
    assertEquals("010000A000001000", result.getNrCompteBeneficiaire());
    assertEquals("010000B025001000", result.getNrCompteEmetteur());
    assertEquals(new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime(), result.getDate());
  }
}