package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.List;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.validator.TransferValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransferService {

  private final Logger LOGGER = LoggerFactory.getLogger(TransferService.class);


  private final AccountRepository accountRepository;
  private final TransferRepository transferRepository;
  private final AuditService auditService;

  public TransferService(
      final TransferRepository transferRepository,
      final AuditService auditService,
      final AccountRepository accountRepository) {
    this.transferRepository = transferRepository;
    this.auditService = auditService;
    this.accountRepository = accountRepository;
  }

  public List<TransferDto> listTransfers() {
    return TransferMapper.map(transferRepository.findAll());
  }

  @Transactional
  public void createTransaction(TransferDto transferDto) throws CompteNonExistantException, TransactionException {

    TransferValidator.validateTransfer(transferDto);

    Account transmitterAccount = accountRepository.findByNrCompte(transferDto.getNrCompteEmetteur())
        .orElseThrow(() -> {
          LOGGER.error("Compte Non existant");
          return new CompteNonExistantException("Compte Non existant");
        });
    Account receiverAccount = accountRepository
        .findByNrCompte(transferDto.getNrCompteBeneficiaire()).orElseThrow(() -> {
          LOGGER.error("Compte Non existant");
          return new CompteNonExistantException("Compte Non existant");
        });

    if (transmitterAccount.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
      LOGGER.error("Solde insuffisant pour l'utilisateur");
      throw new TransactionException("Solde insuffisant pour l'utilisateur");
    }

    transmitterAccount.setSolde(transmitterAccount.getSolde().subtract(transferDto.getMontant()));
    accountRepository.save(transmitterAccount);

    receiverAccount.setSolde(new BigDecimal(receiverAccount.getSolde().intValue() + transferDto.getMontant().intValue()));
    accountRepository.save(receiverAccount);

    Transfer transfer = new Transfer();
    transfer.setDateExecution(transferDto.getDate());
    transfer.setCompteBeneficiaire(receiverAccount);
    transfer.setCompteEmetteur(transmitterAccount);
    transfer.setMontantTransfer(transferDto.getMontant());
    transferRepository.save(transfer);

    auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
        .toString());

  }

}
