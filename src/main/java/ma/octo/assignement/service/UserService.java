package ma.octo.assignement.service;

import java.util.List;
import ma.octo.assignement.dto.UserDto;
import ma.octo.assignement.mapper.UserMapper;
import ma.octo.assignement.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  private final UserRepository userRepository;

  public UserService(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public List<UserDto> listAllUsers() {
    return UserMapper.map(userRepository.findAll());
  }
}
