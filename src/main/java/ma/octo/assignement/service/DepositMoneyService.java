package ma.octo.assignement.service;

import ma.octo.assignement.domain.Account;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositMoneyDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.AccountRepository;
import ma.octo.assignement.repository.DepositMoneyRepository;
import ma.octo.assignement.validator.DepositValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.math.BigDecimal;
@Service
public class DepositMoneyService {

    private final Logger LOGGER = LoggerFactory.getLogger(DepositMoneyService.class);
    private final AccountRepository accountRepository;
    private final DepositMoneyRepository depositMoneyRepository;
    private final AuditService auditService;

    public DepositMoneyService(
            AccountRepository accountRepository,
            DepositMoneyRepository depositMoneyRepository, AuditService auditService) {
        this.accountRepository = accountRepository;
        this.depositMoneyRepository = depositMoneyRepository;
        this.auditService = auditService;
    }

    @Transactional
    public void createdepositmoney(DepositMoneyDto depositMoneyDto) throws CompteNonExistantException, TransactionException{

        DepositValidator.validateDeposit(depositMoneyDto);
        Account recieveraccount = accountRepository.findByRib(depositMoneyDto.getRib()).
                orElseThrow(() -> {
                    LOGGER.error("Ce rib Non existant");
                    return new CompteNonExistantException("Ce rib Non existant");
                });
        recieveraccount.setSolde(new BigDecimal(recieveraccount.getSolde().intValue() + depositMoneyDto.getMontant().intValue()));
        accountRepository.save(recieveraccount);

        MoneyDeposit moneyDeposit = new MoneyDeposit();
        moneyDeposit.setMotifDeposit(depositMoneyDto.getMotifDeposit());
        moneyDeposit.setMontant(depositMoneyDto.getMontant());
        moneyDeposit.setCompteBeneficiaire(depositMoneyDto.getAccountBeneficiaire());
        moneyDeposit.setDateExecution(depositMoneyDto.getDateExecution());
        moneyDeposit.setNom_prenom_emetteur(depositMoneyDto.getNom_prenom_emetteur());
        depositMoneyRepository.save(moneyDeposit);

        auditService.auditDeposit("Depot depuis " + depositMoneyDto.getNom_prenom_emetteur() + " vers " + depositMoneyDto.getRib()
                + " d'un montant de " + depositMoneyDto.getMontant()
                .toString());


    }
}
