package ma.octo.assignement.service;

import java.util.List;
import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.mapper.AccountMapper;
import ma.octo.assignement.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
  @Autowired
  private final AccountRepository accountRepository;

  public AccountService(final AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  public List<AccountDto> listAllAccounts() {
    return AccountMapper.map(accountRepository.findAll());

  }

}
