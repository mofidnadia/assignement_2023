package ma.octo.assignement.dto;

import ma.octo.assignement.domain.Account;


import java.math.BigDecimal;
import java.util.Date;

public class DepositMoneyDto {

    private Long id;
    private BigDecimal montant;
    private Date dateExecution;
    private String nom_prenom_emetteur;
    private Account accountBeneficiaire;
    private String motifDeposit;
    private String rib;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public Date getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

    public String getNom_prenom_emetteur() {
        return nom_prenom_emetteur;
    }

    public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
        this.nom_prenom_emetteur = nom_prenom_emetteur;
    }

    public Account getAccountBeneficiaire() {
        return accountBeneficiaire;
    }

    public void setAccountBeneficiaire(Account accountBeneficiaire) {
        this.accountBeneficiaire = accountBeneficiaire;
    }

    public String getMotifDeposit() {
        return motifDeposit;
    }

    public void setMotifDeposit(String motifDeposit) {
        this.motifDeposit = motifDeposit;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }
}
