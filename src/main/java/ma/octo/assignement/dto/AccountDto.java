package ma.octo.assignement.dto;

import java.math.BigDecimal;

public class AccountDto {

  private Long id;
  private String nrCompte;
  private String rib;
  private BigDecimal solde;
  private String username;

  public AccountDto(final Long id, final String nrCompte, final String rib, final BigDecimal solde, final String username) {
    this.id = id;
    this.nrCompte = nrCompte;
    this.rib = rib;
    this.solde = solde;
    this.username = username;
  }

  public AccountDto() {
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getNrCompte() {
    return nrCompte;
  }

  public void setNrCompte(final String nrCompte) {
    this.nrCompte = nrCompte;
  }

  public String getRib() {
    return rib;
  }

  public void setRib(final String rib) {
    this.rib = rib;
  }

  public BigDecimal getSolde() {
    return solde;
  }

  public void setSolde(final BigDecimal solde) {
    this.solde = solde;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }
}
