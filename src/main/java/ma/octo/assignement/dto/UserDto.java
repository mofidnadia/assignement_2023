package ma.octo.assignement.dto;

import java.util.Date;

public class UserDto {

  private Long id;

  private String username;

  private String gender;

  private String lastname;

  private String firstname;

  private Date birthdate;

  public UserDto() {
  }

  public Long getId() {
    return id;
  }

  public void setId(final Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(final String gender) {
    this.gender = gender;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(final String lastname) {
    this.lastname = lastname;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(final String firstname) {
    this.firstname = firstname;
  }

  public Date getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(final Date birthdate) {
    this.birthdate = birthdate;
  }
}
