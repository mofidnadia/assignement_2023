package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;

public class TransferMapper {

    public static List<TransferDto> map(List<Transfer> transfers) {
        return transfers.stream()
            .map(transfer -> map(transfer))
            .collect(Collectors.toList());
    }

    public static TransferDto map(Transfer transfer) {
        TransferDto transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setMontant(transfer.getMontantTransfer());
        return transferDto;
    }
}
