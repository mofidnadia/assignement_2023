package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.AccountDto;

public class AccountMapper {

  public static List<AccountDto> map(final List<Account> accounts) {
    return accounts.stream().map(account -> map(account)).collect(Collectors.toList());
  }

  private static AccountDto map(final Account account) {
    final AccountDto accountDto = new AccountDto();
    accountDto.setId(account.getId());
    accountDto.setRib(account.getRib());
    accountDto.setNrCompte(account.getNrCompte());
    accountDto.setSolde(account.getSolde());
    accountDto.setUsername(account.getUtilisateur().getUsername());
    return accountDto;
  }
}
