package ma.octo.assignement.mapper;

import java.util.List;
import java.util.stream.Collectors;
import ma.octo.assignement.domain.User;
import ma.octo.assignement.dto.UserDto;

public class UserMapper {

  public static List<UserDto> map(final List<User> users) {
    return users.stream()
        .map(user -> map(user))
        .collect(Collectors.toList());
  }

  private static UserDto map(final User user) {
    final UserDto userDto = new UserDto();
    userDto.setId(user.getId());
    userDto.setFirstname(user.getFirstname());
    userDto.setBirthdate(user.getBirthdate());
    userDto.setGender(user.getGender());
    userDto.setLastname(user.getLastname());
    userDto.setUsername(user.getUsername());
    return userDto;
  }

}
