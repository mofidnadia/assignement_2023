package ma.octo.assignement.web;

import java.util.Collections;
import java.util.List;
import ma.octo.assignement.domain.Account;
import ma.octo.assignement.dto.AccountDto;
import ma.octo.assignement.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/accounts")
public class AccountController {

  private final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

  @Autowired
  private final AccountService accountService;

  public AccountController(final AccountService accountService) {
    this.accountService = accountService;
  }

  @GetMapping("listOfAccounts")
  List<AccountDto> listAllAccounts() {
    LOGGER.info("List accounts");
    final List<AccountDto> accounts = accountService.listAllAccounts();

    return CollectionUtils.isEmpty(accounts) ? Collections.emptyList() : accounts;
  }
}
