package ma.octo.assignement.web;


import ma.octo.assignement.dto.DepositMoneyDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositMoneyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController("/deposit")
public class DepositMoneyController {
    private final Logger LOGGER = LoggerFactory.getLogger(DepositMoneyController.class);

    private final DepositMoneyService depositMoneyService ;

    public DepositMoneyController(DepositMoneyService depositMoneyService) {
        this.depositMoneyService = depositMoneyService;
    }

    @PostMapping("/executeDeposit")
    @ResponseStatus(HttpStatus.CREATED)
    public void createDepositMoney(@RequestBody DepositMoneyDto depositMoneyDto)
            throws CompteNonExistantException, TransactionException {
        LOGGER.info("create deposit from {} to {}", depositMoneyDto.getNom_prenom_emetteur(), depositMoneyDto.getRib());


        depositMoneyService.createdepositmoney(depositMoneyDto);


    }
}
