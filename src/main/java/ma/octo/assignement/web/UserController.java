package ma.octo.assignement.web;

import java.util.Collections;
import java.util.List;
import ma.octo.assignement.dto.UserDto;
import ma.octo.assignement.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/users")
public class UserController {

  private final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  private final UserService userService;

  public UserController(final UserService userService) {
    this.userService = userService;
  }

  @GetMapping("lister_utilisateurs")
  List<UserDto> listAllUsers() {
    LOGGER.info("list users");

    final List<UserDto> users = userService.listAllUsers();

    return CollectionUtils.isEmpty(users) ? Collections.emptyList() : users;
  }
}
