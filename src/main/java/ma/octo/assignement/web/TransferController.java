package ma.octo.assignement.web;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "/transfers")
class TransferController {

    private final Logger LOGGER = LoggerFactory.getLogger(TransferController.class);

    private final TransferService transferService;

    TransferController(TransferService transferService) {
        this.transferService = transferService;
    }

    @GetMapping("listDesTransferts")
    List<TransferDto> listTransfers() {
        LOGGER.info("List transfers");
        var transfers = transferService.listTransfers();
        return CollectionUtils.isEmpty(transfers) ? Collections.emptyList() : transfers;
    }

    @PostMapping("/executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws CompteNonExistantException, TransactionException {

        LOGGER.info("create transaction from {} to {}", transferDto.getNrCompteEmetteur(), transferDto.getNrCompteBeneficiaire());

        transferService.createTransaction(transferDto);

    }


}
