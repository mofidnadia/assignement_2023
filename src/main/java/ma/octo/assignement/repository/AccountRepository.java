package ma.octo.assignement.repository;

import java.util.Optional;
import ma.octo.assignement.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
  Optional<Account> findByNrCompte(String nrCompte);
  Optional<Account> findByRib(String rib);
}
