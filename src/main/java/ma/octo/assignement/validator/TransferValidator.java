package ma.octo.assignement.validator;

import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class TransferValidator {

  private static final int MONTANT_MAXIMAL = 10_000;

  final static Logger LOGGER = LoggerFactory.getLogger(TransferValidator.class);

  public static void validateTransfer(TransferDto transferDto) throws TransactionException {
    if (transferDto.getMontant() == null || transferDto.getMontant().intValue() == 0) {
      LOGGER.info("Montant vide");
      throw new TransactionException("Montant vide");
    } else if (transferDto.getMontant().intValue() < 10) {
      LOGGER.info("Montant minimal de transfer non atteint");
      throw new TransactionException("Montant minimal de transfer non atteint");
    } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
      LOGGER.info("Montant maximal de transfer dépassé");
      throw new TransactionException("Montant maximal de transfer dépassé");
    }

    if (!StringUtils.hasLength(transferDto.getMotif())) {
      LOGGER.info("Motif vide");
      throw new TransactionException("Motif vide");
    }
  }
}
