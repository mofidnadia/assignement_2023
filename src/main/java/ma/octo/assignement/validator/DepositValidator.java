package ma.octo.assignement.validator;
import ma.octo.assignement.dto.DepositMoneyDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
public class DepositValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DepositValidator.class);
    private static final int MONTANT_MAXIMAL=10000;
    public static void validateDeposit(DepositMoneyDto depositMoneyDto) throws TransactionException {

        if (depositMoneyDto.getMontant() == null || depositMoneyDto.getMontant().intValue() == 0) {
            LOGGER.info("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (depositMoneyDto.getMontant().intValue() < 0) {
            LOGGER.info("Montant doit etre superieure de 0");
            throw new TransactionException("Montant doit etre superieure de 0");
        } else if (depositMoneyDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            LOGGER.info("Montant maximal de depot dépassé");
            throw new TransactionException("Montant maximal de depot dépassé");
        }

        if (!StringUtils.hasLength(depositMoneyDto.getMotifDeposit())) {
            LOGGER.info("Motif vide");
            throw new TransactionException("Motif vide");
        }
        if (!StringUtils.hasLength(depositMoneyDto.getNom_prenom_emetteur())) {
            System.out.println("Nom et prénom vide");
            throw new TransactionException("Nom et prénom vide");
        }
    }

}
